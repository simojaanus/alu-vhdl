--Function 1 A, B cnt 1
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity Function1 is
	port(
		A_IN, B_IN	:	in std_logic_vector(3 downto 0);
		OUT_SIG		:	out std_logic_vector(3 downto 0));
end Function1;

architecture FUNCTION1 of Function1 is
begin

FUNCTION1: process(A_IN, B_IN)
variable count : unsigned(3 downto 0) ;

begin

count := "0000"; 

for i in 0 to 3 loop
if(A_IN(i) = '1') then
count := count + 1;
end if;
if(B_IN(i) = '1') then
count := count + 1;
end if;
end loop;
OUT_SIG <= std_logic_vector(count);
end process FUNCTION1;
end FUNCTION1;