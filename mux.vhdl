library IEEE;
use IEEE.std_logic_1164.all;
entity Mux is
	port(
		F1_IN,F2_IN,F3_IN,F4_IN	:	in std_logic_vector(3 downto 0);
		CONT_SIG					:	in std_logic_vector(1 downto 0);
		OUT_SIG					:	out std_logic_vector(3 downto 0));
end Mux;

architecture MUX of Mux is
begin
MUX: process(F1_IN,F2_IN,F3_IN,F4_IN, CONT_SIG)
begin
case CONT_SIG is
when "00" => OUT_SIG <= F1_IN;
when "01" => OUT_SIG <= F2_IN;
when "10" => OUT_SIG <= F3_IN;
when "11" => OUT_SIG <= F4_IN;
when others => OUT_SIG <= "XXXX";
end case;
	
end process MUX;
end MUX;