--Function 2 rol A (ringnihe vasakule)
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all; 
entity Function2 is
	port(
		A_IN	:	in std_logic_vector(3 downto 0);
		OUT_SIG		:	out std_logic_vector(3 downto 0));
end Function2;

architecture FUNCTION2 of Function2 is

begin

FUNCTION2: process(A_IN)
--https://forums.xilinx.com/t5/Synthesis/ROL-logic-operator-library/m-p/126774#M3468
begin
OUT_SIG  <= std_logic_vector(rotate_left(unsigned(A_IN), 1));
end process FUNCTION2;
end FUNCTION2;
