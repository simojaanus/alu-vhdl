--Function 4 Kodune t��
library IEEE;
use IEEE.std_logic_1164.all;
entity Function4 is
	port(
		A_IN	:	in std_logic_vector(3 downto 0);
		OUT_SIG		:	out std_logic_vector(3 downto 0));
end Function4;

architecture FUNCTION4 of Function4 is
begin
FUNCTION4: process(A_IN)
begin
	if A_IN="0011" then
		OUT_SIG<= "0001";
	elsif A_IN="1000" then
		OUT_SIG<= "0001";
	elsif A_IN="1001" then
		OUT_SIG<= "0001";
	elsif A_IN="1011" then
		OUT_SIG<= "0001";
	else 
		OUT_SIG<= "0000";
		end if;
		

end process FUNCTION4;
end FUNCTION4;