--ALU design
--Control signal is CONT_SIG
--ALU Entity and architecture
library IEEE, func1, func2, func3, fun3, mux;
use IEEE.std_logic_1164.all;
entity Alu is
	port(
		A_IN, B_IN			:	in std_logic_vector(3 downto 0);
		CONT_SIG				:	in std_logic_vector(1 downto 0);
		OUT_SIG				:	out std_logic_vector(3 downto 0));
end Alu;

architecture ALU of Alu is

component Function1 is
	port(
		A_IN, B_IN	:	in std_logic_vector(3 downto 0);
		OUT_SIG		:	out std_logic_vector(3 downto 0));
end component;

component Function2 is
	port(
		A_IN	:	in std_logic_vector(3 downto 0);
		OUT_SIG		:	out std_logic_vector(3 downto 0));
end component;

component Function3 is
	port(
		A_IN, B_IN	:	in std_logic_vector(3 downto 0);
		OUT_SIG		:	out std_logic_vector(3 downto 0));
end component;

component Function4 is
	port(
		A_IN	:	in std_logic_vector(3 downto 0);
		OUT_SIG		:	out std_logic_vector(3 downto 0));
end component;

--Mux component
component Mux is
	port(
		F1_IN,F2_IN,F3_IN,F4_IN	:	in std_logic_vector(3 downto 0);
		CONT_SIG					:	in std_logic_vector(1 downto 0);
		OUT_SIG					:	out std_logic_vector(3 downto 0));
end component;

signal f1,f2,f3,f4	:	std_logic_vector(3 downto 0);

begin
--Structural description ALU
F1	:	Function1 port map (A_IN, B_IN, f1);
F2	:	Function2 port map (A_IN, f2);
F3	:	Function3 port map (A_IN, B_IN, f3);
F4	:	Function4 port map (A_IN, f4);
MUX	:	Mux port map(f1, f2, f3 ,f4, CONT_SIG, OUT_SIG);

end ALU;


