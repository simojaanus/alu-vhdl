LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;

--Testbench entity
entity AluTestBench is
end AluTestbench;

architecture Bench of AluTestBench is

 --Component declaration for ALU
component Alu is
	port(
		A_IN, B_IN, CONT_SIG	:	in std_logic_vector(3 downto 0);
		OUT_SIG				:	out std_logic_vector(3 downto 0));
end component;

--Local signal declarations
--Signal "Answer" is not a part of the ALU, it is only used for comparing results.
signal AInTB,BInTB,OutSigTB,Answer	:	std_logic_vector(3 downto 0);
signal ContSigTB				:	std_logic_vector(1 downto 0);
begin
--Component instantiation of ALU
ALU: Alu port map (A_IN => AInTB, B_IN => BInTB, CONT_SIG => ContSigTB, OUT_SIG => OutSigTB);

--Stimulus process
Stimulus: process
	begin
		--Test F1: A, B cnt 1
		ContSigTB<="00";
		AInTB<="1100";
		BInTB<="0111";
		Answer<="0101";
		wait for 5 ns;
		
		AInTB<="1111";
		BInTB<="1111";
		Answer<="1000";
		wait for 5 ns;
		
		AInTB<="1110";
		BInTB<="0111";
		Answer<="0110";
		wait for 5 ns;

		AInTB<="1111";
		BInTB<="1111";
		Answer<="1000";
		wait for 5 ns;
		
		
		
		--Test F2: rol A 
		ContSigTB<="01";
		AInTB<="1001";
		Answer<="0011";
		wait for 5 ns;
		
		AInTB<="1111";
		Answer<="1111";
		wait for 3 ns;
		
		AInTB<="0010";
		Answer<="0100";
		wait for 5 ns;

		AInTB<="1011";
		Answer<="0111";
		wait for 5 ns;
		
		
		
		--Test F3: xor A, B 
		ContSigTB<="10";		
		AInTB<="0110";
		BInTB<="0001";
		Answer<="0100";
		wait for 5 ns;
		
		AInTB<="0100";
		BInTB<="0001";
		Answer<="0110";
		wait for 5 ns;
		
		AInTB<="0100";
		BInTB<="0110";
		Answer<="0000";
		wait for 5 ns;

		AInTB<="1111";
		BInTB<="0100";
		Answer<="1110";
		wait for 5 ns;
		
		
		
		--Test F4
		ContSigTB<="11";
		AInTB<="1010";
		Answer<="0000";
		wait for 5 ns;
		
		AInTB<="0101";
		Answer<="0000";
		wait for 5 ns;
		
		AInTB<="0000";
		Answer<="0000";
		wait for 5 ns;

		AInTB<="1000";
		Answer<="0001";
		wait for 5 ns;
		
		wait;  --Suspend
	end process Stimulus;

end Bench;