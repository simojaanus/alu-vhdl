--Function 3 xor A, B (inverteerida s�na A B-nda biti v��rtus)
library IEEE;
use IEEE.std_logic_1164.all;
entity Function3 is
	port(
		A_IN, B_IN	:	in std_logic_vector(3 downto 0);
		OUT_SIG		:	out std_logic_vector(3 downto 0));
end Function3;

architecture FUNCTION3 of Function3 is
begin

FUNCTION3: process(A_IN, B_IN)
begin
	if B_IN="0000" or B_IN="0100" or B_IN="1100" or B_IN="1000"then
		OUT_SIG <= A_IN(3 downto 1) & not(A_IN(0 downto 0));
	elsif B_IN="0001" or B_IN="0101" or B_IN="1101" or B_IN="1001" then
		OUT_SIG <= A_IN(3 downto 2) & not(A_IN(1 downto 1)) & A_IN(0 downto 0);
	elsif B_IN="0010" or B_IN="0110" or B_IN="1110" or B_IN="1010" then
		OUT_SIG <= A_IN(3 downto 3) & not(A_IN(2 downto 2)) & A_IN(1 downto 0);
	elsif B_IN="0011" or B_IN="0111" or B_IN="1111" or B_IN="1011" then
		OUT_SIG <= not(A_IN(3 downto 3)) & A_IN(2 downto 0);
	end if;
end process FUNCTION3;
end FUNCTION3;